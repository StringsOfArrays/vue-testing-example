// https://docs.cypress.io/api/introduction/api.html

// this describes the user flow
describe('Test the main page from a user perspective', () => {
  // first, lets route to the main page
  it('Can successfully open the main Page', () => {
    cy.visit('/');
  });

  // the user should see a counter, initialized to 0
  it('Has a counter set to 0', () => {
    cy.get('[data-testid=counter-val]').should('have.text', '0');
  });

  // the user sees an increment and a decrement button
  it('Has an increment and decrement button', () => {
    cy.contains('button', 'increment');
    cy.contains('button', 'decrement');
  });

  // when pressing the increment button, the counter will change to 1
  it('Increases the counter by 1 when pressing increment', () => {
    cy.get('[data-testid=increment-btn]').click();
    cy.get('[data-testid=counter-val]').should('have.text', '1');
  });

  // when pressing the decrement button, th counter gets subtracted by 1 and returns to be 0
  it('Decreases the counter by 1 when pressing decrement', () => {
    cy.get('[data-testid=decrement-btn]').click();
    cy.get('[data-testid=counter-val]').should('have.text', '0');
  });
});
