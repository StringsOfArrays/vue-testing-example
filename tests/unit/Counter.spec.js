import { render, fireEvent } from '@testing-library/vue';
import Counter from '../../src/components/Counter.vue';

test('increment value on click', async () => {
  // render component to test
  const { getByTestId } = render(Counter);

  // cache elements by testid
  const counter = getByTestId('counter-val');
  const incBtn = getByTestId('increment-btn');

  // start testing ...
  expect(counter.innerHTML).toBe('0');
  await fireEvent.click(incBtn);
  expect(counter.innerHTML).toBe('1');
});

test('decrement value on click', async () => {
  // render component to test
  const { getByTestId } = render(Counter);

  // cache elements by testid
  const counter = getByTestId('counter-val');
  const decBtn = getByTestId('decrement-btn');

  // start testing ...
  expect(counter.innerHTML).toBe('0');
  await fireEvent.click(decBtn);
  expect(counter.innerHTML).toBe('-1');
});
